var request = require('request');
var cheerio = require('cheerio');
var firebase = require("firebase");

// FIREBASE

var config = {
  apiKey: "AIzaSyCpOMKcs-pp3bvsj4oKJ0fqTsjGzb5WDc0",
  authDomain: "optimat-a99fd.firebaseapp.com",
  databaseURL: "https://optimat-a99fd.firebaseio.com",
  storageBucket: "optimat-a99fd.appspot.com",
  messagingSenderId: "1095622408577"
};
firebase.initializeApp(config);

var database = firebase.database();


// SCRAPE SV

url = 'http://www.optimaedu.fi/svenska/unga/studieguiden/lunchmeny.html';
request(url, function(error, response, html) {
  if (!error && response.statusCode == 200) {
    var $ = cheerio.load(html);
    var menu = {};
    $('div.mainText p:has(br)').each(function(i, element) {
      $('span').remove();
      var food = $(this).text();
      menu[i] = food
    })
    firebase.database().ref('food/sv/').set({
      menu
    });
    console.log(menu);
  }
});


// SCRAPE FI

request(url, function(error, response, html) {
  if (!error && response.statusCode == 200) {
    var $ = cheerio.load(html);
    var menu = {};
    $('div.mainText p > span').each(function(i, element) {
      var food = $(this).first().text();
      menu[i] = food
    })
    firebase.database().ref('food/fi/').set({
      menu
    });
    console.log(menu);
//    database.goOffline();
  }
});
